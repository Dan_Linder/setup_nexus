# Testing formatting of YAML in markdown

First: Basic YAML formatting.

```yaml
all:
  children:
    ######
    # Group: AutomationController
    automationcontroller:
      hosts:
        gdb1std02-xxx01.ent.{{ ansible_domain }}: {}
        gdb1std02-xxx02.ent.{{ ansible_domain }}: {}
        gdb1std02-xxx03.ent.{{ ansible_domain }}: {}
      vars:
        peers: execution_nodes
        rhel8stig_white_list_services:
          - 591/tcp
          - 27199/tcp
          - ssh
          - http
          - https
        vg_name: awx
        vg_disk: sdb
        app_volumes:
          - mount_point: '/var/lib/awx'
            size: "150g"
            fstype: xfs
        cert_alias:
          - rhaap
          - rhaap.ent.{{ ansible_domain }}
```

Second: Can we add line numbers?

```yml
all:
  children:
    ######
    # Group: AutomationController
    automationcontroller:
      hosts:
        gdb1std02-xxx01.ent.{{ ansible_domain }}: {}
        gdb1std02-xxx02.ent.{{ ansible_domain }}: {}
        gdb1std02-xxx03.ent.{{ ansible_domain }}: {}
      vars:
        peers: execution_nodes
        rhel8stig_white_list_services:
          - 591/tcp
          - 27199/tcp
          - ssh
          - http
          - https
        vg_name: awx
        vg_disk: sdb
        app_volumes:
          - mount_point: '/var/lib/awx'
            size: "150g"
            fstype: xfs
        cert_alias:
          - rhaap
          - rhaap.ent.{{ ansible_domain }}
```